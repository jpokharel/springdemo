package org.jiwan.springtutorial;

//import org.springframework.beans.factory.BeanFactory;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class DrawingApp {

	public static void main(String[] args) {
		
		//Now this has been linked to bitbucket.
		
		@SuppressWarnings("resource")
		//Provides the Bean XML path and creates the Bean through BeanFactory
		//BeanFactory: TO instantiate the Object of our own.
		//BeanFactory factory=new ClassPathXmlApplicationContext("/org/jiwan/springtutorial/spring.xml");
		//Using application Context to replace the above line of code, which are equivalent.
		ApplicationContext factory=new ClassPathXmlApplicationContext("spring.xml"); 
		//When we put spring file somewhere else we need to provide relative path eg: /org/jiwan/springtutorial/ or classpath:.....
		//After implementing interface, we can use Generic call to interface.
		
		Shape shapeTriangle=(Shape)(factory.getBean("triangle"));
		Shape shapeCircle=(Shape)(factory.getBean("circle"));
		shapeTriangle.draw();
		shapeCircle.draw();
		
	   	//Triangle triangle=(Triangle) (factory.getBean("triangle"));
		//triangle.draw();

	}

}
