package org.jiwan.springtutorial;

//import java.util.List;

import org.springframework.beans.BeansException;
import org.springframework.beans.factory.BeanNameAware;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;

//These implements Interfaces are added for additional portions of Aware Methods
public class Triangle implements ApplicationContextAware,BeanNameAware,Shape {
	
	//To Implement ApplicationContextAware, used when the scope is "Prototype".
	
	private Point pointA;
	private Point pointB;
	private Point pointC;
	private ApplicationContext context=null; //=null; //done to take the context from Setter method.
	//We can also use the name of the bean as member variable here as:
	// private String beanName;
	
	public Point getPointA() {
		return pointA;
	}
	public void setPointA(Point pointA) {
		this.pointA = pointA;
	}
	public Point getPointB() {
		return pointB;
	}
	public void setPointB(Point pointB) {
		this.pointB = pointB;
	}
	public Point getPointC() {
		return pointC;
	}
	public void setPointC(Point pointC) {
		this.pointC = pointC;
	}
	
	public void draw(){
		System.out.println("For Triangle, First Point is: (" + getPointA().getX() + " , " + getPointA().getY() + " )");
		System.out.println("For Triangle, Second Point is: (" + getPointB().getX() + " , " + getPointB().getY() + " )");
		System.out.println("For Triangle, Third Point is: (" + getPointC().getX() + " , " + getPointC().getY() + " )");
	}
	
	@Override
	public void setBeanName(String beanName) {
		// Here we would set the bean name that we get from the XML file and can use wherever we 
		//want to use the bean name (In case we don't know what is the name of the bean in XML.
		//But we are just printing the name of the bean here.
		System.out.println("Bean name is: "+beanName);
		
	}
	
	@Override
	public void setApplicationContext(ApplicationContext context) throws BeansException {
		this.context=context; //Context refers to the DrawingApp Class.
		
	}
	
	
	//To generate list of Point Objects.
	/*
	List<Point> points;

	public List<Point> getPoints() {
		return points;
	}

	public void setPoints(List<Point> points) {
		this.points = points;
	}
	
	public void draw(){
		for (Point point : points)
		System.out.println("Point: ("+point.getX()+" , "+point.getY()+" )");
	}
	*/

	//Obtain point from each (x,y)
	//This is to look for Object data type
	/*
	private Point pointA;
	private Point pointB;
	private Point pointC;
	
	public Point getPointA() {
		return pointA;
	}
	public void setPointA(Point pointA) {
		this.pointA = pointA;
	}
	public Point getPointB() {
		return pointB;
	}
	public void setPointB(Point pointB) {
		this.pointB = pointB;
	}
	public Point getPointC() {
		return pointC;
	}
	public void setPointC(Point pointC) {
		this.pointC = pointC;
	}
	
	public void draw(){
		System.out.println("First Point is: (" + getPointA().getX() + " , " + getPointA().getY() + " )");
		System.out.println("Second Point is: (" + getPointB().getX() + " , " + getPointB().getY() + " )");
		System.out.println("Third Point is: (" + getPointC().getX() + " , " + getPointC().getY() + " )");
	}
	*/
	
	//This was the primitive data-type example
	/*
	private String type;
	private int height;
	
	
	public Triangle(String type) {
		this.type=type;
	}

	public Triangle(int height) {
		this.height=height;
	}
	
	public Triangle(String type, int height) {
		this.type = type;
		this.height = height;
	}

	//Only getter used to make use of <constructor-arg>.
	public int getHeight() {
		return height;
	}

	public String getType() {
		return type;
	}

	//Used when the <property> tag is used.
	/*
	public void setType(String type) {
		this.type = type;
	}
	*/
	/*
	public void draw(){
		System.out.println("This is a triangle of type: "+getType()
		+" with the height:"+getHeight());
	}
	
	*/
}
