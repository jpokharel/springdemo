package org.jiwan.springtutorial;

public class Circle implements Shape{

	private Point center;
	
	@Override
	public void draw() {
		System.out.println("Center of Circle is: ("+center.getX()+" , "+center.getY()+" )");
		
	}

	public Point getCenter() {
		return center;
	}

	public void setCenter(Point center) {
		this.center = center;
	}

}
