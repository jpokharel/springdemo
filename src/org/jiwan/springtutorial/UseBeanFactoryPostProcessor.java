package org.jiwan.springtutorial;

import org.springframework.beans.BeansException;
import org.springframework.beans.factory.config.BeanFactoryPostProcessor;
import org.springframework.beans.factory.config.ConfigurableListableBeanFactory;

public class UseBeanFactoryPostProcessor implements BeanFactoryPostProcessor {

	@Override
	public void postProcessBeanFactory(ConfigurableListableBeanFactory context) throws BeansException {
		//Trying to use most of the methods in it.
		System.out.println("Inside the Bean Factory Post Processor: "+context.toString());
		System.out.println("Bean Definition Count is: "+context.getBeanDefinitionCount());
		System.out.println("Bean Post Processor Count is: "+context.getBeanPostProcessorCount());
		System.out.println("Number of Singleton Counts is: "+context.getSingletonCount());
		System.out.println("Hash Code for this is: "+context.hashCode());
		System.out.println("Access Control Context is: "+context.getAccessControlContext());
		System.out.println("Bean Definition Names are: "+context.getBeanDefinitionNames());
		System.out.println("Bean Names Iterator is: "+context.getBeanNamesIterator());
		System.out.println("The class is: "+context.getClass());
		System.out.println("The dependencies for bean 'triangle' are: "+context.getDependenciesForBean("triangle"));
		System.out.println("Cache metadata: "+context.isCacheBeanMetadata());
		System.out.println("Is pointA a prototype bean: "+context.isPrototype("pointA"));
		System.out.println("Is pointA a singleton bean: "+context.isSingleton("pointA"));
			
	}

}
